EL-GY 6483 Lab Materials
====================

This repository contains the materials for the EL-GY 6483 (Real Time Embedded Systems) lab.

We will use `git` in this course, both to share the lab materials with you (the students) and for you to share your submissions with us (the instructors).

To start off, each student should **fork** the lab repository as follows:

 * Visit [Bitbucket.org](https://bitbucket.org/) and sign in; create an account if you don't already have one.

 * Go to [https://bitbucket.org/el6483s2015/el6483-lab/fork](https://bitbucket.org/el6483s2015/el6483-lab/fork) and **fork** the repository for this lab.

  Please use `el6483-netID` (with your own Net ID) as the name for your fork, make your repository private (at least for the duration of this course), and check the box to "Inherit repository user/group permissions." Since we're using git to receive and grade your lab submissions, it's important that you set it up correctly:

 ![Fork repository](http://i.imgur.com/OtUAN0N.png)


Once you have your fork of the lab repository, you can proceed with [Lab 1](https://bitbucket.org/el6483s2015/el6483-lab/src/master/1-blinky/).

## Lab sequence

* [Lab 1: Blinky](https://bitbucket.org/el6483s2015/el6483-lab/src/master/1-blinky/)
* [Lab 2: GPIO Peripherals](https://bitbucket.org/el6483s2015/el6483-lab/src/master/2-peripherals/)
* [Lab 3: Analog to digital conversion](https://bitbucket.org/el6483s2015/el6483-lab/src/master/3-adc-temp/)
* [Lab 4: Accelerometer and SPI](https://bitbucket.org/el6483s2015/el6483-lab/src/master/4-spi-accel/)
* [Lab 5: Timing and Interrupts](https://bitbucket.org/el6483s2015/el6483-lab/src/master/5-timing-interrupts/)
* [Lab 6: RTOS](https://bitbucket.org/el6483s2015/el6483-lab/src/master/6-rtos/)
* [Lab 7: Internet of things](https://bitbucket.org/el6483s2015/el6483-lab/src/master/7-iot/)